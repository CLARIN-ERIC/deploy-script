#! /bin/sh

# ORIGINAL CODE FROM:
# https://github.com/yaccz/git-branch-backup

# Copyright (c) 2017, Jan Matějka (yac@blesmrt.net)
# All rights reserved.

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:

# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

function highest_backup_id {
    local branch="${1}"
    [[ "${branch}" == "" ]] && return 1

    local prefix="${branch}.backup"
    git branch --list --color=never "${prefix}*" \
        | sed "s/^..${prefix}//" \
        | sort -nr \
        | head -n1
}

function new_backup_id {
    local branch="${1}"
    local highest=$(highest_backup_id "${branch}") || return
    echo $(( $highest + 1 ))
}

function main {
    local branch id
    branch=$(git rev-parse --abbrev-ref HEAD) || return

    if [[ "${branch}" == "HEAD" ]] ; then
        echo "There is no branch on HEAD" >&2
        return 1
    fi

    if [[ ${branch} =~ ^.*\.backup[0-9]{1,}$ ]]; then
        echo "HEAD is already a backup" >&2
        return 1
    fi

    id=$(new_backup_id "${branch}") || return

    local backup="${branch}.backup${id}"
    git branch "${backup}" || return
    echo "Created backup branch '${backup}'"
}

main
