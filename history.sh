#!/bin/bash

_DEPLOY_JSON="deploy.json"
NAME="x"
OLD_TAG="test"
NEW_TAG="test2"

#Create initial json file if needed
    if [ ! -f "${_DEPLOY_JSON}" ]; then
        jq -n '{"deploy_history": {}}' > "${_DEPLOY_JSON}"
    fi
    cat "${_DEPLOY_JSON}"

    jq -e ".deploy_history | select(.${NAME} != null)" deploy.json
    if [ "$?" != "0" ]; then
        jq ".deploy_history +=  {\"${NAME}\": []}" "${_DEPLOY_JSON}" > "${_DEPLOY_JSON}.tmp"
        mv "${_DEPLOY_JSON}.tmp" "${_DEPLOY_JSON}"
        cat "${_DEPLOY_JSON}"
    fi


    #Check if project key exists
    jq ".deploy_history.${NAME} +=  [{\"date\": \"1-1-2018\", \"old_tag\": \"${OLD_TAG}\", \"new_tag\": \"${NEW_TAG}\"}]" "${_DEPLOY_JSON}" > "${_DEPLOY_JSON}.tmp"
    mv "${_DEPLOY_JSON}.tmp" "${_DEPLOY_JSON}"
    cat "${_DEPLOY_JSON}"
