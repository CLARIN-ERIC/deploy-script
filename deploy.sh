#!/bin/bash

set -e

shopt -s expand_aliases
[[ -f ~/.bashrc ]] && . ~/.bashrc

_METRICS_ENDPOINT=${METRICS_ENDPOINT:-"collect.metrics.clarin.eu"}
_DEPLOY_JSON_PREFIX="/home/deploy/deploy-assets/deploy"

START=0
RESTART=0
RELOAD=0
HELP=0
VERBOSE=0
NAME=""
GIT_REPO=""
GIT_TAG=""
GIT_OLD_TAG=""
HISTORY=0

#
# Process script arguments
#
while [ $# -gt 0 ]
do
key="$1"
case $key in
    -h|--history)
    HISTORY=1
    ;;
    -n|--name)
    NAME=$2
    shift
    ;;
    -g|--git)
    GIT_REPO=$2
    shift
    ;;
    -t|--tag)
    GIT_TAG=$2
    shift
    ;;
    start)
    START=1
    ;;
    restart)
    RESTART=1
    ;;
    reload)
    RELOAD=1
    ;;
    -h|--help)
    HELP=1
    ;;
    -d|--debug)
    VERBOSE=1
    ;;
    *)
    echo "Unkown option: $key"
    HELP=1
    ;;
esac
shift # past argument or value
done

validate_repo_tag ( ) {
    if [ -z "$(git ls-remote --tags "$1" "refs/tags/$2")" ]; then
        echo "Invalid tag"
        exit 1
    fi
    echo "Remote tag is valid."
}

update_json_history() {
    #Store deploy history locally
    DATE=$(date "+%Y-%m-%d %T")

    _DEPLOY_JSON="${_DEPLOY_JSON_PREFIX}_${NAME}.json"
    #_DEPLOY_JSON="${_DEPLOY_JSON_PREFIX}.json"

    set +e
    #Create json file
    if [ ! -f "${_DEPLOY_JSON}" ]; then
        jq -n '{"deploy_history": {}}' > "${_DEPLOY_JSON}"
    fi

    #Create project key
    jq -e ".deploy_history | select(\".${NAME}\" != null)" "${_DEPLOY_JSON}" > /dev/null
    if [ "$?" != "0" ]; then
        jq ".deploy_history +=  {\"${NAME}\": []}" "${_DEPLOY_JSON}" > "${_DEPLOY_JSON}.tmp"
        mv "${_DEPLOY_JSON}.tmp" "${_DEPLOY_JSON}"
    fi

    #Add history entry to array
    jq ".deploy_history.\"${NAME}\" +=  [{\"date\": \"${DATE}\", \"old_tag\": \"${GIT_OLD_TAG}\", \"new_tag\": \"${GIT_TAG}\", \"git\": \"${GIT_REPO_NAME}\"}]" "${_DEPLOY_JSON}" > "${_DEPLOY_JSON}.tmp"
    mv "${_DEPLOY_JSON}.tmp" "${_DEPLOY_JSON}"
    set -e
}

send_deploy_stats() {
    update_json_history

    #Send deploy statistics to metrics endpoint
    HOSTNAME=$(hostname -s)
    echo "deploys.${GIT_REPO_NAME}.${HOSTNAME}:1|c" | nc -w 1 -u ${_METRICS_ENDPOINT} 8125
}

control_script() {
    CMD="$1"
    (cd /home/deploy && ./control.sh "${NAME}" "${CMD}" || echo "WARNING: control.sh does not support '${CMD}' action. Starting normally...")
}

print_history() {
    _DEPLOY_JSON="${_DEPLOY_JSON_PREFIX}_${NAME}.json"
    jq -c ".deploy_history.\"${NAME}\"[]" "${_DEPLOY_JSON}"
}

# Print parameters if running in verbose mode
if [ "${VERBOSE}" -eq 1 ]; then
    set -x
fi

if [ "${HELP}" -eq 1 ]; then
    echo ""
    echo "Deploy a project:"
    echo ""
    echo "usage: ./deploy.sh --name <name> --git <remote> [start|restart|reload]"
    echo ""
    echo "--name <name>         The name of the local parent director, where the git project will be deployed."
    echo "--git <remote>        The remote URL of the git project. Or alternatively the remote git project name"
    echo "                      if located on 'git@gitlab.com:CLARIN-ERIC/<git_project_name>.git'"
    echo "--tag <tag>           The remote git tag to deploy."
    echo ""
    echo "start                 Start the compose project after deploying."
    echo "restart               Restart the compose project after deploying."
    echo "reload                Reload the compose project after deploying."
    echo ""
    echo "Print deploy history for the specified project name:"
    echo ""
    echo "usage: ./deploy.sh --history --name <name>"
    echo ""
    echo "--name <name>         The name of the local parent director, where the git project will be deployed."
    echo ""
    exit 1
fi

if [ "${NAME}" = "" ]; then
    echo "NAME is required"
    exit 1
fi

if [ ${HISTORY} == "1" ]; then
    print_history
    exit 0
fi

if [ "${GIT_REPO}" = "" ]; then
    echo "GIT_REPO url is required"
    exit 1
fi
if [ "${GIT_TAG}" = "" ]; then
    echo "GIT_TAG is required"
    exit 1
fi

CWD=$(pwd)

GIT_REPO_NAME="${GIT_REPO#*:}"
if [ "${GIT_REPO_NAME}" != "${GIT_REPO}" ]; then
    # If full git URL supplied, parse out repository name
    GIT_REPO_NAME=$(basename "${GIT_REPO_NAME}")
    GIT_REPO_NAME="${GIT_REPO_NAME%.*}"
else
    # If only repository name supplied, build full git URL
    GIT_REPO="git@gitlab.com:CLARIN-ERIC/${GIT_REPO_NAME}.git"
fi

validate_repo_tag "$GIT_REPO" "$GIT_TAG"

echo "Using git repository name: ${GIT_REPO_NAME} on ${GIT_REPO}"

if [ ! -d "${NAME}" ]; then
    echo "Creating ${NAME}"
    mkdir "${NAME}"
    chmod 775 "${NAME}"
    cd "${NAME}"

    echo "Cloning ${URL}"
    git init "${GIT_REPO_NAME}"
    cd "${GIT_REPO_NAME}"
    git remote add origin "${GIT_REPO}"
    git fetch origin --depth=1 "+refs/tags/${GIT_TAG}:refs/tags/${GIT_TAG}"
    git reset --hard "tags/${GIT_TAG}"
    git submodule update --init --recursive
    git submodule foreach --recursive git reset --hard
    git gc --prune=all

    DEPLOYED_TAG=$(git describe --tags)

    #Log deploy history and send deploy stats to central server
    send_deploy_stats

    #Make sure the project is properly initialized
    control_script init
    #Run any additional action if needed
    if [ "${START}" -eq 1 ]; then
        control_script apply-update && control_script start
    fi
else
    echo "Updating ${NAME}"
    SCRIPT_PATH=$(dirname "${BASH_SOURCE[0]}")
    if readlink "$0"  >/dev/null 2>&1; then
        SCRIPT_PATH=$(dirname "$(readlink "$0")")
    fi
    SCRIPT_PATH=$(cd $SCRIPT_PATH && pwd)
    cd "${NAME}"
    cd "${GIT_REPO_NAME}"
    GIT_OLD_TAG=$(git describe --tags)
    # backup current git repo state
    git add .
    git commit -m "Local backup commit" || :
    . "${SCRIPT_PATH}"/git-branch-backup.sh
    # clean previous tags and history
    git reflog expire --expire=all --all
    git tag -l | xargs -r bash -ic 'git tag -d "$@";exit' arg0
    git gc --prune=all
    # fetch and apply specified tag
    git fetch origin --depth=1 "+refs/tags/${GIT_TAG}:refs/tags/${GIT_TAG}"
    git reset --hard "tags/${GIT_TAG}"
    git submodule update
    git submodule foreach --recursive git reset --hard
    # clean git repo
    git gc --prune=all

    #Log deploy history and send deploy stats to central server
    send_deploy_stats

    #cd /home/deploy
    #Make sure the project is properly initialized
    control_script init
    #Run any additional action if needed
    if [ "${START}" -eq 1 ]; then
        control_script apply-update && control_script start
    elif [ "${RESTART}" -eq 1 ]; then
        control_script backup && control_script stop && control_script apply-update && control_script start
    elif [ "${RELOAD}" -eq 1 ]; then
        control_script reload
    fi
fi

cd "${CWD}"
